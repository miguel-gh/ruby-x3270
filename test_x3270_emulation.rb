require 'te3270'
require 'minitest/autorun'
require 'pry'

class MainFrameScreen
  include TE3270

  text_field(:aplicacion, 21, 70, 74)

  def ingresar_aplicacion(app)
    self.aplicacion = app
  end

end

class IBMTest < Minitest::Test
  def setup()
    @emulator = TE3270.emulator_for :x3270 do |platform|
      platform.executable_command = 'x3270 -script' #MUY IMPORTANTE usar modo script!
      platform.host = '<ip>:<port>'
      platform.max_wait_time = 5
      platform.trace = true
    end
  end

  def test_conexion_x3270
    # Esta es una manera directa de enviar Info
    #@emulator.put_string("hola", 21,70)
    #@emulator.send_keys(TE3270.Enter)
    #var = @emulator.get_string(21, 70, 4)

    # Esta es una forma OO de enviar info. Haciendo Objetos por cada
    # pantalla, indicando cada (row,column) por cada input
    my_screen = MainFrameScreen.new(@emulator)
    my_screen.ingresar_aplicacion('pro1')

    @emulator.screenshot('img.txt') # Toma screenshots en formato texto

    binding.pry
  end
end
